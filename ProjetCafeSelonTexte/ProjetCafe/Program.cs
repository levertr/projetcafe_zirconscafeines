﻿using System;
using System.Text;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets



namespace cafaitlecafe
{
    class Program
    {
        public static byte[] ConnectionSocket(string host, int port, Socket MaSocket)
        {

            //test de connection avec recuperation de l'exception en cas d'erreur 
            try
            {
                Console.WriteLine("Establishing Connection to {0}", host);
                MaSocket.Connect(host, port);
                Console.WriteLine("Connection established");
                Console.WriteLine("  ");
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Declaration du tableau recevant les bytes correspondant a la carte 
            byte[] macarte = new byte[1000];

            return macarte;
        }
        static void Affiche(byte[] lamap, Socket MaSocket)
        {
            //declarations et insertions du nombre de bytes envoyé par le serveur pour l'affichage
            int byteCount = MaSocket.Receive(lamap, 0, MaSocket.Available, SocketFlags.None);


            //affichage des bytes et donc de la carte récupérer sur le serveur
            for (int i = 0; i < byteCount; i++)
            {
                Convert.ToChar(lamap[i]);
                Console.Write(lamap[i]);
                ASCIIEncoding asen = new ASCIIEncoding();
            }
        }

        static int[,] decoupeTrameEnMatrice(string file) // fonction qui decoupe la trame recue en matrice ayant chaque valeur 
        {
            int i = 0;
            string[] fileCoupeParLigne = file.Split('|'); // découpe le trame afin d'avoir chaque ligne
            int[,] fileCoupeParValeurs = new int[10, 10]; // créé une matrice de 10x10

            for (var j = 0; j < 10; j++) 
            {
                string[] fileCoupe4 = fileCoupeParLigne[j].Split(':'); // découpe chaque ligne afin d'avoir chaque valeur
                i = 0;
                foreach (var lines in fileCoupe4)
                {
                    fileCoupeParValeurs[j, i] = int.Parse(lines); // convertit chaque valeur en entier et le stocke dans la matrice
                    i++;
                }
            }
            return fileCoupeParValeurs; // retourne la matrice remplie
        }

        static char[,] determineTypeParcelle(int[,] matriceValeurs) // fonction retournant la lettre signifiant le type de parcelle
        {
            int i = 0, j = 0;
            char[,] matriceTypeParcelle = new char[10, 10];
                foreach (var valeurs in matriceValeurs)
                {
                if (valeurs <= 79 && valeurs >= 64)
                {
                    matriceTypeParcelle[j, i] = 'M'; // retourne M pour Mer
                }
                else if (valeurs <= 47 && valeurs >= 32)
                {
                    matriceTypeParcelle[j, i] = 'F'; // retourne F pour Forêt
                }
                else
                {

                }
                    i++;

                    if(i == 10)
                    {
                        i = 0;
                        j++;
                    }
                }
            return matriceTypeParcelle;
        }

        static void afficheMap(char[,] lettresParcelles) // procédure affichant la carte selon la trame 
        {
            int i = 0;
            foreach(var lettre in lettresParcelles) // boucle pour chaque lettre issue de la matrice
            {
                Console.Write(lettre + " ");
                i++;
                if(i == 10)
                {
                    Console.WriteLine();
                    i = 0;
                }
            }
        }   
        static void Main(string[] args)
        {
            //Déclaration variables contenant l'ip et le port auxquels se connecter
            string Lip = "172.16.0.88";
            int Leport = 1212;
            byte[] lamaprecup = new byte[1000];
            var rand = new Random();
            int randomInt = rand.Next(0, 9);
            //Déclaration variables contenant la lecture du fichier et la matrice contenant les valeurs
            string file = System.IO.File.ReadAllText("trame.txt");
            int[,] fileCoupeParValeurs = new int[10, 10];
            char[,] matriceTypeParcelle = new char[10, 10];

            fileCoupeParValeurs = decoupeTrameEnMatrice(file); // appel de la fonction retournant la matrice remplie par la trame passé par un texte

            matriceTypeParcelle =  determineTypeParcelle(fileCoupeParValeurs); // appel de la fonction retournant la lettre signifiant le type de parcelle

            afficheMap(matriceTypeParcelle); // appel de la procédure affichant la carte

            //Declarations de ma socket en ipv4 et protocole TCP
            //Socket MaSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


            //Appel de la fonction connection au server
            //lamaprecup = ConnectionSocket(Lip, Leport, MaSocket);

            //fonction Affiche des bytes de la map 
            //Affiche(lamaprecup, MaSocket);

            Console.ReadKey();
        }

    }
}
