﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace ProjetCafe
{
    class Program
    {
        static void Main(string[] args)
        {
            //Déclaration variables contenant l'ip et le port auxquels se connecter
            string ip_server = "127.0.0.1";
            int port_server = 1213;
            byte[] la_map_recup = new byte[1000];

            //Déclaration variables contenant la lecture du fichier et la matrice contenant les valeurs
            Unite[,] unite_coupe_par_valeurs = new Unite[10, 10];
            char[,] tableau_type_unite = new char[10, 10];
            
            //Declarations de ma socket en ipv4 et protocole TCP
            Socket MaSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //Appel de la fonction connection au server
            la_map_recup = ConnectionWithServer.ConnectionSocket(ip_server, port_server, MaSocket);

            //fonction Affiche des bytes de la map
            ConnectionWithServer.Affiche(la_map_recup, MaSocket);

            string messageRecu = ConnectionWithServer.recoieDuServer(MaSocket); // appel de la fonction recevant la trame du serveur

            unite_coupe_par_valeurs = DetermineCarte.decoupeTrameEnMatrice(messageRecu); // appel de la fonction retournant la matrice remplie par la trame passé par un texte

            tableau_type_unite = DetermineCarte.determineTypeUniteDeBase(unite_coupe_par_valeurs); // appel de la fonction créant la map de base sans modifications 

            tableau_type_unite = DetermineCarte.determineTypeUniteFinale(unite_coupe_par_valeurs, tableau_type_unite); // appel de la fonction retournant la matrice finale exacte

            /*ConnectionWithServer.afficheMap(tableau_type_unite);  // affiche la carte selon les lettres de la matrice*/

            // partie pour le semestre 4 IA

            //Déclaration des variables permettant le fonctionnement de l'IA
            Boolean partieFinie = true, premierTour = true;
            string[] messagesDuServer = new string[3];
            Joueur IAServer = new Joueur(0,0); Joueur Client = new Joueur(0,0);
            int[] grainesPlacees = new int[56]; 
            int compteurGraine = 0, i, j;

            Console.WriteLine("");

            while (partieFinie) // dès que le client reçoit "FINI" du serveur
            {
                i = 0; j = 0; 
                Console.Write("Veuillez écrire vos coordonnées où vous voulez situer votre graine ( X et Y ) : ");

                foreach (var valeurs in tableau_type_unite) // premier tour = 1er unité disponible ( a )
                {
                    if (j == 10) { i++; j = 0; } // passage à la ligne suivante
                    if (premierTour && valeurs == 'a') // dès le premier a rencontré, la graine est placée
                    {
                        ConnectionWithServer.setInfosClient(Client, i, j, valeurs);
                        grainesPlacees[compteurGraine] = i * 10 + j; compteurGraine++; // fusion de la ligne et de la colonne pour former un seul entier
                        premierTour = false;
                        break;
                    }

                    if (i == Client.getGraineLigne() || i == IAServer.getGraineLigne() && !premierTour) // placer la graine à la même ligne que le précedent tour
                    {
                        if(valeurs != 'M' && valeurs != 'F' && valeurs != Client.getDerniereLettre() && valeurs != IAServer.getDerniereLettre()) // placer la graine différent de la parcelle du tour précedent
                        {
                            if (!grainesPlacees.Contains(i * 10 + j)) // placer la graine dans une unité vide
                            {
                                ConnectionWithServer.setInfosClient(Client, i, j, valeurs);
                                grainesPlacees[compteurGraine] = i * 10 + j; compteurGraine++;
                                break;
                            }
                        }
                    }
                    else if (j == IAServer.getGraineColonne() && !premierTour) // placer la graine à la même colonne que le précedent tour
                    {
                        if (valeurs != 'M' && valeurs != 'F' && valeurs != Client.getDerniereLettre() && valeurs != IAServer.getDerniereLettre()) // placer la graine différent de la parcelle du tour précedent
                        {
                            if (!grainesPlacees.Contains(i * 10 + j)) // placer la graine dans une unité vide
                            {
                                ConnectionWithServer.setInfosClient(Client, i, j, valeurs);
                                grainesPlacees[compteurGraine] = i * 10 + j; compteurGraine++;
                                break;
                            }
                        }
                    }
                    j++;
                }

                string graine_entree = "A:" + Client.getGraineLigne() + Client.getGraineColonne(); // création du code pour l'envoi au serveur
                Console.WriteLine(graine_entree + " parcelle " + Client.getDerniereLettre());
                ConnectionWithServer.envoieVersLeServer(MaSocket, graine_entree);

                messagesDuServer[0] = ConnectionWithServer.recoieDuServer(MaSocket); // recoit du server VALI ou INVA
                messagesDuServer[1] = ConnectionWithServer.recoieDuServer(MaSocket); // recoit du server B:xy ou FINI 
                messagesDuServer[2] = ConnectionWithServer.recoieDuServer(MaSocket); // recoit du server ENCO ou FINI

                Console.ReadKey();

                if (messagesDuServer[0] == "VALI") // si le coup est validé par le serveur
                {
                    Console.WriteLine("Votre coup est correct ! ");
                }
                else Console.WriteLine("Votre coup est incorrect"); // si le coup n'est pas validé

                if (messagesDuServer[1] == "FINI" || messagesDuServer[2] == "FINI") // si la partie se finit
                {
                    string score = ConnectionWithServer.recoieDuServer(MaSocket);
                    ConnectionWithServer.affichageScore(score);
                    partieFinie = false;
                }
                else // si la partie est toujours d'actualité
                {
                    i = 0; j = 0;
                    IAServer.setGraineLigne(int.Parse(messagesDuServer[1].Substring(2, 1))); // récupération de la ligne de la graine de l'IA
                    IAServer.setGraineColonne(int.Parse(messagesDuServer[1].Substring(3))); // récupération de la colonne de la graine de l'IA

                    foreach(var valeurs in tableau_type_unite) // récupération de la lettre de la graine de l'IA
                    {
                        if(i == IAServer.getGraineLigne() && j == IAServer.getGraineColonne()) // obtention du choix du serveur
                        { 
                            IAServer.setDerniereLettre(valeurs);
                            grainesPlacees[compteurGraine] = i * 10 + j; compteurGraine++; // fusion de la ligne et de la colonne pour former un seul entier
                            break;
                        }
                        j++;
                        if (j == 10) { i++; j = 0; }
                    }
                    Console.WriteLine("L'adversaire a placé sa graine à " + messagesDuServer[1] + " parcelle " + IAServer.getDerniereLettre());
                }
            }
            Console.WriteLine("Appuyez sur une touche du clavier pour quitter le jeu");
            Console.ReadKey();
        }
    }
}
