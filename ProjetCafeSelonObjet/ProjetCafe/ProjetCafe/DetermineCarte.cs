﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjetCafe
{
    class DetermineCarte
    {
        public static Unite typeEtFrontiere(int valeur)
        {
            int type = 0;
            bool bordureEst = false, bordureNord = false, bordureSud = false, bordureOuest = false;
            if (valeur / 64 == 1) { type = 64; valeur -= 64; }
            if (valeur / 32 == 1) { type = 32; valeur -= 32; }
            if (valeur / 8 == 1) { valeur -= 8; bordureEst = true; }
            if (valeur / 4 == 1) { valeur -= 4; bordureSud = true; }
            if (valeur / 2 == 1) { valeur -= 2; bordureOuest = true; }
            if (valeur / 1 == 1) { valeur -= 1; bordureNord = true; }

            Unite unite = new Unite(valeur, type, bordureEst, bordureNord, bordureSud, bordureOuest);
            return unite;
        }
        public static Unite[,] decoupeTrameEnMatrice(string string_lecture) // fonction qui decoupe la trame recue en matrice ayant chaque valeur 
        {
            int i = 0;
            string[] string_coupe_par_lignes = string_lecture.Split('|'); // découpe le trame afin d'avoir chaque ligne
            Unite[,] unite_coupe_par_valeurs = new Unite[10, 10]; // créé un tableau de 10x10

            for (var j = 0; j < 10; j++)
            {
                string[] string_coupe = string_coupe_par_lignes[j].Split(':'); // découpe chaque ligne afin d'avoir chaque valeur
                i = 0;
                foreach (var valeurs in string_coupe)
                {
                    unite_coupe_par_valeurs[j, i] = typeEtFrontiere(int.Parse(valeurs)); // initialise pour chaque unite ses bordures en fonction de sa valeur
                    unite_coupe_par_valeurs[j, i].setValeur(int.Parse(valeurs)); // convertit chaque valeur en entier et le stocke dans la matrice
                    i++;
                }
            }
            return unite_coupe_par_valeurs; // retourne la matrice remplie
        }

        public static char[,] determineTypeUniteDeBase(Unite[,] unite_coupe_par_valeurs) // fonction retournant la lettre signifiant le type d'unité de base
        {
            int i = 0, j = 0;
            char[,] tableau_type_unite = new char[10, 10];

            foreach (var valeurs in unite_coupe_par_valeurs)
            {
                if (valeurs.getValeur() <= 79 && valeurs.getValeur() >= 64)
                {
                    tableau_type_unite[j, i] = 'M'; // retourne M pour Mer
                }
                else if (valeurs.getValeur() <= 47 && valeurs.getValeur() >= 32)
                {
                    tableau_type_unite[j, i] = 'F'; // retourne F pour Forêt
                }
                else
                {
                    tableau_type_unite[j, i] = 'a'; // retourne a pour initialiser une graine
                }
                i++;
                if (i == 10)
                {
                    i = 0;
                    j++;
                }
            }
            return tableau_type_unite;
        }

        public static char[,] determineTypeUniteFinale(Unite[,] matriceValeurs, char[,] tableau_type_unite)
        {
            char lettre = 'a';
            for (var j = 0; j < 9; j++)
            {
                for (var k = 0; k < 9; k++)
                {
                    if (matriceValeurs[j, k].getType() == 0) // si l'unite est une graine
                    {
                        if (matriceValeurs[j, k].getBordureEst() == true) // si l'unite a une bordure Est
                        {
                            if (matriceValeurs[j, k + 1].getType() != 0) { }
                            else if(matriceValeurs[j - 1, k + 1].getBordureSud() == false) { }
                            else
                            {
                                lettre++;
                                tableau_type_unite[j, k + 1] = lettre; // on change de lettre pour l'unite à Est
                            }
                        }
                        else tableau_type_unite[j, k + 1] = tableau_type_unite[j, k]; // sinon l'unite Ouest prend la meme lettre
                        if (matriceValeurs[j, k].getBordureSud() == true) // si l'unite a une bordure Sud
                        {
                            if (matriceValeurs[j + 1, k].getType() != 0) { }
                            else tableau_type_unite[j + 1, k] = lettre; // on change la lettre pour l'unite au Sud
                        }
                        else tableau_type_unite[j + 1, k] = tableau_type_unite[j, k]; // sinon l'unite Nord prend la meme lettre
                    }
                }
            }
            return tableau_type_unite;
        }
    }
}
