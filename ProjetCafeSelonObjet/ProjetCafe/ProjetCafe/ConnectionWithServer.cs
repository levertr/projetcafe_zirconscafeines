﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets

namespace ProjetCafe
{
    class ConnectionWithServer
    {
        public static byte[] ConnectionSocket(string ip_server, int port_server, Socket MaSocket)
        {

            //test de connection avec recuperation de l'exception en cas d'erreur 
            try
            {
                Console.WriteLine("Establishing Connection to {0}", ip_server);
                MaSocket.Connect(ip_server, port_server);
                Console.WriteLine("Connection established");
                Console.WriteLine("  ");
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Declaration du tableau recevant les bytes correspondant a la carte 
            byte[] la_map_recup = new byte[1000];

            return la_map_recup;
        }

        public static void Affiche(byte[] la_map_recup, Socket MaSocket)
        {
            //declarations et insertions du nombre de bytes envoyé par le serveur pour l'affichage
            Unite[,] unite_coupe_par_valeurs = new Unite[10, 10];
            char[,] tableau_type_unite = new char[10, 10];
            int byteCount = MaSocket.Receive(la_map_recup, 0, MaSocket.Available, SocketFlags.None);
            string trame = Encoding.ASCII.GetString(la_map_recup, 0, byteCount);
        }

        public static void afficheMap(char[,] tableau_type_unite)
        {
            int i = 0;
            foreach (var lettre in tableau_type_unite)
            {
                Console.Write(lettre + " ");
                i++;
                if (i == 10)
                {
                    Console.WriteLine();
                    i = 0;
                }
            }
        }

        public static void envoieVersLeServer(Socket socket, string message)
        {
            byte[] messageEnvoye = Encoding.ASCII.GetBytes(message);
            socket.Send(messageEnvoye);
        }

        public static string recoieDuServer(Socket socket)
        {
            byte[] messageRecuEnByte = new byte[1000];
             int byteCount = socket.Receive(messageRecuEnByte);
            string messageRecuEnString = Encoding.ASCII.GetString(messageRecuEnByte, 0, byteCount);
            return messageRecuEnString;
        }

        public static void affichageScore(string chaine)
        {
            int pointsClient = int.Parse(chaine.Substring(2, 2));
            int pointsIA = int.Parse(chaine.Substring(5));
            Console.WriteLine("La partie est terminée");
            Console.WriteLine("Votre score est de : " + pointsClient);
            Console.WriteLine("Le score de l'adversaire est de : " + pointsIA);

            if (pointsIA > pointsClient)
            {
                Console.WriteLine("L'IA a gagné la partie !");
            } else Console.WriteLine("Le joueur A a gagné la partie !");
        }

        public static void setInfosClient(Joueur Client, int i, int j, char valeurs)
        {
            Client.setGraineLigne(i);
            Client.setGraineColonne(j);
            Client.setDerniereLettre(valeurs);
        }
    }
    }
