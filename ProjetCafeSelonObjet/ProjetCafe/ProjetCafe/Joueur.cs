﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetCafe
{
    /*Classe "Joueur" définissant ce que les deux IA ont comme propriétes afin de jouer une partie*/
    class Joueur
    {
        int graineColonne, graineLigne; // attributs définissant où l'IA a choisi de placer sa graine
        char derniereLettre; // attribut définissant la lettre de parcelle où l'IA a placé sa graine

        public Joueur(int graineLigne, int graineColonne) // constructeur de base de ce qu'est un joueur pour le programme
        {
            this.graineColonne = graineColonne;
            this.graineLigne = graineLigne;
        }

        public void setGraineLigne(int valeur) { graineLigne = valeur; }
        public void setGraineColonne(int valeur) { graineColonne = valeur; }
        public int getGraineLigne() { return graineLigne; }
        public int getGraineColonne() { return graineColonne; }
        public void setDerniereLettre(char valeur) { derniereLettre = valeur; }
        public char getDerniereLettre() { return derniereLettre; }
    }
}
