﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetCafe
{
    class Unite
    {
        bool bordureEst, bordureNord, bordureSud, bordureOuest;
        int type; // 0 = terre, 32 = Foret, 64 = Mer
        int valeur = 0; 
        public Unite()
        {
            this.valeur = 0;
            this.bordureEst = false; this.bordureNord = false; this.bordureSud = false; this.bordureOuest = false;
            this.type = 0;
        }

        public Unite(int valeur, int type, bool bordureEst, bool bordureNord, bool bordureSud, bool bordureOuest)
        {
            this.valeur = valeur;
            this.bordureEst = bordureEst; this.bordureNord = bordureNord; this.bordureSud = bordureSud; this.bordureOuest = bordureOuest;
            this.type = type;
        }
        
        public int getType() { return type; }
        public bool getBordureNord() { return bordureNord; }
        public bool getBordureEst() { return bordureEst; }
        public bool getBordureOuest() { return bordureOuest; }
        public bool getBordureSud() { return bordureSud; }
        public int getValeur() { return valeur; }
        public void setValeur(int valeur) { this.valeur = valeur; }
        public void toString() { Console.WriteLine("valeur = {0}, type = {1}, bordureEst = {2}, bordureNord = {3}, bordureSud = {4}, bordureOuest = {5}", valeur, type, bordureEst, bordureNord, bordureSud, bordureOuest); }
    }
}
