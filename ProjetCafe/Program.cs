﻿using System;
using System.Text;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets



namespace cafaitlecafe
{
    class Program
    {
        public static byte[] ConnectionSocket(string host, int port, Socket MaSocket)
        {

            //test de connection avec recuperation de l'exception en cas d'erreur 
            try
            {
                Console.WriteLine("Establishing Connection to {0}", host);
                MaSocket.Connect(host, port);
                Console.WriteLine("Connection established");
                Console.WriteLine("  ");
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Declaration du tableau recevant les bytes correspondant a la carte 
            byte[] macarte = new byte[1000];

            return macarte;
        }
        static void Affiche(byte[] lamap, Socket MaSocket)
        {
            //declarations et insertions du nombre de bytes envoyé par le serveur pour l'affichage
            int byteCount = MaSocket.Receive(lamap, 0, MaSocket.Available, SocketFlags.None);


            //affichage des bytes et donc de la carte récupérer sur le serveur
            for (int i = 0; i < byteCount; i++)
            {
                Convert.ToChar(lamap[i]);
                Console.Write(lamap[i]);
                ASCIIEncoding asen = new ASCIIEncoding();
            }
        }

        static int[,] decoupeTrameEnMatrice(string file) // fonction qui decoupe la trame recue en matrice ayant chaque valeur 
        {
            int i = 0;
            string[] fileCoupeParLigne = file.Split('|'); // découpe le trame afin d'avoir chaque ligne
            int[,] fileCoupeParValeurs = new int[10, 10]; // créé une matrice de 10x10

            for (var j = 0; j < 10; j++) 
            {
                string[] fileCoupe4 = fileCoupeParLigne[j].Split(':'); // découpe chaque ligne afin d'avoir chaque valeur
                i = 0;
                foreach (var lines in fileCoupe4)
                {
                    fileCoupeParValeurs[j, i] = int.Parse(lines); // convertit chaque valeur en entier et le stocke dans la matrice
                    i++;
                }
            }
            return fileCoupeParValeurs; // retourne la matrice remplie
        }

        static char[,] determineTypeParcelle(int[,] matriceValeurs) // fonction retournant la lettre signifiant le type de parcelle
        {
            int i = 0, j = 0;
            char[,] matriceTypeParcelle = new char[10, 10];
                foreach (var valeurs in matriceValeurs)
                {
                if (valeurs <= 79 && valeurs >= 64)
                {
                    matriceTypeParcelle[j, i] = 'M'; // retourne M pour Mer
                }
                else if (valeurs <= 47 && valeurs >= 32)
                {
                    matriceTypeParcelle[j, i] = 'F'; // retourne F pour Forêt
                }
                else 
                {
                    matriceTypeParcelle[j, i] = 'a'; // retourne a pour initialiser une graine
                }
                    i++;

                    if(i == 10)
                    {
                        i = 0;
                        j++;
                    }
                }
            return matriceTypeParcelle;
        }

        static char[,] determineTypeParcelleFinale(int[,] matriceValeurs, char[,] matriceTypeParcelle)
        {
            int i = 0;
            char lettre = 'a';
            char lettreSave;
            bool change = false;
            for (var j = 0; j < 10; j++)
            {
                i = 0;
                for(var k = 0; k <10; k++)
                {
                    /* if(matriceValeurs[j,i] >=8 && matriceValeurs[j, i] <= 14)
                    {
                        if(matriceTypeParcelle[j,i+1] == 'M' || matriceTypeParcelle[j, i + 1] == 'F')
                        {
                            
                        }
                        else matriceTypeParcelle[j, i + 1] = 'b';
                    }
                    */
                    
                    if (matriceValeurs[j, i] >= 0 && matriceValeurs[j, i] <= 16)
                    {

                        if (matriceValeurs[j, i] / 8 == 1) // east
                        {
                            matriceValeurs[j, i] -= 8;
                            if (matriceTypeParcelle[j, i + 1] == 'M' || matriceTypeParcelle[j, i + 1] == 'F')
                            {

                            }
                            else
                            {
                                if(change == false)
                                {

                                }
                                else
                                {
                                    lettre++;
                                    matriceTypeParcelle[j, i + 1] = lettre;
                                    change = false;
                                }
                              
                            }
                        }
                        else
                        {
                            if (matriceTypeParcelle[j, i + 1] == 'M' || matriceTypeParcelle[j, i + 1] == 'F')
                            {

                            }
                            else matriceTypeParcelle[j, i + 1] = matriceTypeParcelle[j, i];
                        }

                        
                        if (matriceValeurs[j, i] / 4 == 1) // south
                        {
                            matriceValeurs[j, i] -= 4;
                            if (matriceTypeParcelle[j + 1, i] == 'M' || matriceTypeParcelle[j, i + 1] == 'F')
                            {

                            }
                            else matriceTypeParcelle[j + 1, i] = lettre;
                        }
                        else
                        {
                            if (matriceTypeParcelle[j + 1, i] == 'M' || matriceTypeParcelle[j, i + 1] == 'F')
                            {

                            }
                            else
                            {
                                matriceTypeParcelle[j + 1, i] = matriceTypeParcelle[j, i];
                                lettreSave = matriceTypeParcelle[j, i];
                                change = true;
                            }
                        }

                        /*
                        if (matriceValeurs[j, i] / 2 == 1) // west
                        {
                            //matriceValeurs[j, i] -= 2;
                            if (matriceTypeParcelle[j, i - 1] == 'M' || matriceTypeParcelle[j, i + 1] == 'F')
                            {

                            }
                            else matriceTypeParcelle[j, i - 1] = 'w';
                        }
                        else
                        {
                            if (matriceTypeParcelle[j, i - 1] == 'M' || matriceTypeParcelle[j, i - 1] == 'F')
                            {

                            }
                            else matriceTypeParcelle[j, i - 1] = matriceTypeParcelle[j, i];
                        }

                        if (matriceValeurs[j, i] / 1 == 1) // north
                        {
                            //matriceValeurs[j, i] -= 1;
                            if (matriceTypeParcelle[j - 1, i] == 'M' || matriceTypeParcelle[j, i + 1] == 'F')
                            {

                            }
                            else matriceTypeParcelle[j - 1, i] = 'n';
                        }
                        else
                        {
                            if (matriceTypeParcelle[j - 1, i] == 'M' || matriceTypeParcelle[j - 1, i] == 'F')
                            {

                            }
                            else matriceTypeParcelle[j - 1, i] = matriceTypeParcelle[j, i];
                        }
                        */
                    }

                    i++;
                }
            }
            return  matriceTypeParcelle;
        }

        static void afficheMap(char[,] lettresParcelles)
        {
            int i = 0;
            foreach(var lettre in lettresParcelles)
            {
                Console.Write(lettre + " ");
                i++;
                if(i == 10)
                {
                    Console.WriteLine();
                    i = 0;
                }
            }
        }   
        static void Main(string[] args)
        {
            //Déclaration variables contenant l'ip et le port auxquels se connecter
            string Lip = "172.16.0.88";
            int Leport = 1212;
            byte[] lamaprecup = new byte[1000];
            var rand = new Random();
            int randomInt = rand.Next(0, 9);
            //Déclaration variables contenant la lecture du fichier et la matrice contenant les valeurs
            int[,] fileCoupeParValeurs = new int[10, 10];
            char[,] matriceTypeParcelle = new char[10, 10];
            string file = "trameEcrite.txt";
            

            //Declarations de ma socket en ipv4 et protocole TCP
            //Socket MaSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //Appel de la fonction connection au server
            //lamaprecup = ConnectionSocket(Lip, Leport, MaSocket);

            //fonction Affiche des bytes de la map
            //Affiche(lamaprecup, MaSocket);

            //Ecrit la trame recue par le serveur dans un fichier
            //System.IO.File.WriteAllBytes(file, lamaprecup);

            //Lit le fichier contenant la trame
            string fileLecture = System.IO.File.ReadAllText(file);

            fileCoupeParValeurs = decoupeTrameEnMatrice(fileLecture); // appel de la fonction retournant la matrice remplie par la trame passé par un texte

            matriceTypeParcelle =  determineTypeParcelle(fileCoupeParValeurs); // appel de la fonction retournant la lettre signifiant le type de parcelle

            matriceTypeParcelle = determineTypeParcelleFinale(fileCoupeParValeurs, matriceTypeParcelle); // appel de la fonction retournant la matrice contenant les lettres

            afficheMap(matriceTypeParcelle);  // affiche la carte selon les lettres de la matrice

            Console.ReadKey();
        }

    }
}
